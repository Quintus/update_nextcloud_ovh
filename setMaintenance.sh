#!/bin/bash

mv config.php config.php.back

firstLine=true;
while IFS= read -r line
do
    case "$line" in *maintenance*)
        line="  'maintenance' => $1,"
    esac;
    if $firstLine
        then
            outFile="${outFile}${line}"
            firstLine=false
        else
            outFile="${outFile}"\\n"${line}"
    fi;
done < config.php.back
echo -e "$outFile" > config.php